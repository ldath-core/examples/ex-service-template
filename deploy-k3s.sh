#!/usr/bin/env bash
set -e
export APP_NAME=ex-service-template
k3d image import -c bookCluster $APP_NAME:latest
helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-$APP_NAME ex-book/ex-book-helm-chart --version 0.3.2 --dry-run --debug
#helm upgrade -i -f secrets/k3s-values.yaml --namespace=services k3s-$APP_NAME ex-book/ex-book-helm-chart --version 0.3.2 --wait
